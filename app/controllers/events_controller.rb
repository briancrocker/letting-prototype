class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :verify_authenticity_token, :only => :create

  # GET /events
  # GET /events.json
  def index
    @events = Event.where(start: params[:start]..params[:end])
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  def new
    @event = Event.new
  end

  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.save
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    @event.update(event_params)

  end

  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:title, :start, :end, :event_type, :notes, :property_id, :user_id)
    end
end
