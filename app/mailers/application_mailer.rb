class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@cityletsplymouth.co.uk'
  layout 'mailer'
end
