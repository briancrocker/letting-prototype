json.extract! meeting, :id, :name, :start_time, :created_at, :updated_at
json.url event_url(meeting, format: :json)
