# json.array! @events, partial: "events/event", as: :event
json.array! @events do |event|
  # date_format = '%Y-%m-%dT%H:%M:%S'
  json.id event.id
  json.user event.user.username
  json.title event.title
  json.start event.start
  json.end event.end
  # json.color event.color unless event.color.blank?
  # json.allDay event.all_day_event? ? true : false
  json.update_url event_path(event, method: :patch)
  json.edit_url edit_event_path(event)
end