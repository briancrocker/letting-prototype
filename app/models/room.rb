class Room < ApplicationRecord
  belongs_to :property
  belongs_to :landlord

  def room_details
    "#{number} #{title} #{price}"
  end
end
