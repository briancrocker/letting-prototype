class Landlord < ApplicationRecord
  has_many :properties
  has_many :rooms, through: :properties

  def landlord_name
    "#{first_name} #{surname}"
  end
end
