class Property < ApplicationRecord
  has_many :rooms
  has_many :viewings
  has_many :events, through: :viewings
  has_one :landlord
  has_one_attached :property_image

  serialize :property_type, Array

  def property_name
    "#{number} #{street}"
  end

end
