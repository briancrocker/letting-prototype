class Event < ApplicationRecord
  has_many :viewings
  has_many :properties, through: :viewings
  belongs_to :user


  # validate :expiration_date_cannot_be_in_the_past
  validates :title, presence: true
  # validate :end_time_after_start_time?
  attr_accessor :date_range


  # def end_time_after_start_time?
  #   if :end < :start
  #     errors.add :end, "must be after start time"
  #   end
  # end

  def expiration_date_cannot_be_in_the_past
    if expiration_date.present? && expiration_date < Date.today
      errors.add(:end, "can't be in the past")
    end
  end

  def event_name
    if event_type == 1
      event_name = 'Viewing'
    elsif event_type == 2
      event_name = 'Maintainence'
    elsif event_type == 3
      event_name = 'Landlord check'
    elsif event_type == 4
      event_name = 'Staff Holiday'
    end
  end

  # def all_day_event?
  #   self.start == self.start.midnight && self.end == self.end.midnight ? true : false
  # end
end
