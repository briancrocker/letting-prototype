class CreateViewings < ActiveRecord::Migration[5.2]
  def change
    create_table :viewings do |t|
      t.references :event, foreign_key: true
      t.references :property, foreign_key: true

      t.timestamps
    end
  end
end
