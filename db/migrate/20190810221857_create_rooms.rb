class CreateRooms < ActiveRecord::Migration[5.1]
  def change
    create_table :rooms do |t|
      t.string :number
      t.string :title
      t.text :description
      t.date :tenancy_start
      t.date :tenancy_end
      t.decimal :price
      t.boolean :let_status, default: 0
      t.references :property, foreign_key: true
      t.references :landlord, foreign_key: true

      t.timestamps
    end
  end
end
