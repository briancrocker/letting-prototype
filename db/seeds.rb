# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#

properties = [
  [ '15', 'Cherry Park', 'Plympton', 'Plymouth', 'Devon', 'PL7 1PF' ],
  [ '742', 'Evergreen Terrace', 'Mutley Plain', 'Plymouth', 'Devon', 'PL4 4FE' ],
  [ '222 B', 'Baker St,', 'City Centre', 'Plymouth', 'Devon', 'PL1 1SH' ],
  [ '1600', 'Pennsylvania Avenue', 'Stoke', 'Plymouth', 'Devon', 'PL2 3GF' ],
  [ '1', 'Sutherland Rd (Coach House)', '', 'Plymouth', '', '' ],
  [ '2', 'Sutherland Rd (Coach House)', '', 'Plymouth', '', '' ],
  [ '1', 'Sutherland Rd (LGFF) 1', '', 'Plymouth', '', '' ],
  [ '1', 'Sutherland Rd Flat 2', '', 'Plymouth', '', '' ],
  [ '1', 'Sutherland Rd Flat 3', '', 'Plymouth', '', '' ],
  [ '1', 'Sutherland Rd Flat 4', '', 'Plymouth', '', '' ],
  [ '1', 'Sutherland Rd Flat 5', '', 'Plymouth', '', '' ],
  [ '1', 'Sutherland Rd Flat 6', '', 'Plymouth', '', '' ],
  [ '1', 'Sutherland Rd Flat 7', '', 'Plymouth', '', '' ],
  [ '3', 'Crozier Road', '', 'Plymouth', '', '' ],
]
properties.each do |number, street, address_line_2, city, county, postcode|
  Property.create( number: number,
                   street: street,
                   address_line_2: address_line_2,
                   city: city,
                   county: county,
                   postcode: postcode )
end

#
### ---------------
#
landlords = [
  [ 'Mr', 'Brian', 'Crocker', '01234567891', '', 'brian@kineticjam.co.uk', '15 Cherry Park, Plympton, Plymouth' ],
  [ 'Mr', 'James', 'Jamerson', '01234567891', '', 'james@woo.co.uk', '15 Beech Road, Plymouth' ],
  [ 'Mr', 'Homer', 'Simpson', '01234567891', '', 'homer@slumlord.com', '742 Evergreen Terrace, Mutley Plain' ]
]
landlords.each do |title, first_name, surname, contact_number, alternate_number, email, address|
  Landlord.create( title: title,
                   first_name: first_name,
                   surname: surname,
                   contact_number: contact_number,
                   alternate_number: alternate_number,
                   email: email,
                   address: address )
end
#
### ---------------
#
rooms = [
  [ '1', 'Ground Floor Flat', 'Tidy flat, large living space, enough room for student parties.', '2019-09-01', '2020-08-31', '105', 1, 1, 1 ],
  [ '2', 'First Floor Flat', 'Nice flat, large living space, enough room for student parties.', '2019-09-01', '2020-08-31', '100', 1, 1, 1 ],
  [ '3', 'Top Floor Flat', 'Luxury flat, large living space, enough room for student parties.', '2019-09-01', '2020-08-31', '115', 0, 1, 1 ],
  [ '1', 'Flat A', 'Proin risus dolor, finibus sed nibh et, hendrerit volutpat lectus. Nulla facilisi. Sed eu egestas ante.', '2019-09-01', '2020-08-31', '115', 1, 2, 2 ],
  [ '2', 'Flat B', 'Proin risus dolor, finibus sed nibh et, hendrerit volutpat lectus. Nulla facilisi. Sed eu egestas ante.', '2019-09-01', '2020-08-31', '115', 1, 2, 2 ],
  [ '1', 'Top Floor Flat', 'Proin risus dolor, finibus sed nibh et, hendrerit volutpat lectus. Nulla facilisi. Sed eu egestas ante.', '2019-09-01', '2020-08-31', '115', 0, 3, 1 ],
]
rooms.each do |number, title, description, tenancy_start, tenancy_end, price, property_id, landlord_id|
  Room.create( number: number,
               title: title,
               description: description,
               tenancy_start: tenancy_start,
               tenancy_end: tenancy_end,
               price: price,
               property_id: property_id,
               landlord_id: landlord_id )
end

#
### ---------------
#
users = [
  ['Brian', 'Crocker', 'briancrocker', 'brian@test.com', 'password', 'password', 1],
  ['James', 'Jamerson', 'jamesjamerson', 'james@woo.co.uk', 'JamesJamerson1', 'JamesJamerson1', 1],
  ['Gary', 'Castlehouse', 'garycastlehouse', 'gary@woo.com', 'password', 'password', 1],
  ['Carl', 'Carlson', 'carlcarlson', 'carl@city.co.uk', 'password', 'password', 0],
  ['Lenny', 'Leonard', 'lenny', 'lenny@city.co.uk', 'password', 'password', 0]
]
users.each do |first_name, surname, username, email, password, password_confirmation, admin|
  User.create( first_name: first_name,
               surname: surname,
               username: username,
               email: email,
               password: password,
               password_confirmation: password_confirmation,
               admin: admin )
end

#
### ---------------
#
events = [
  ['Viewing with new students', DateTime.parse("09/10/2019 14:00"), DateTime.parse("09/10/2019 15:00"), 1, 1, 1],
  ['Fix leaky tap', DateTime.parse("09/10/2019 09:00"), DateTime.parse("09/10/2019 12:00"), 2, 1, 3],
  ['Viewing on large property', DateTime.parse("21/10/2019 09:00"), DateTime.parse("21/10/2019 10:00"), 1, 2, 4],
  ['Viewing on large property', DateTime.parse("10/10/2019 10:00"), DateTime.parse("10/10/2019 11:00"), 1, 2, 3],
  ['Viewing on large property', DateTime.parse("10/10/2019 11:00"), DateTime.parse("10/10/2019 15:00"), 1, 2, 5],
  ['Viewing on large property', DateTime.parse("10/10/2019 12:00"), DateTime.parse("10/10/2019 13:00"), 1, 2, 1],
  ['Electrician Visit', DateTime.parse("18/10/2019 09:00"), DateTime.parse("18/10/2019 12:00"), 2, 1, 3],
  ['Viewing with new students', DateTime.parse("15/10/2019 14:00"), DateTime.parse("15/10/2019 15:00"), 1, 10, 4],
  ['Fix leaky tap', DateTime.parse("15/10/2019 09:00"), DateTime.parse("15/10/2019 12:00"), 2, 1, 5],
  ['Viewing on large property', DateTime.parse("15/10/2019 09:00"), DateTime.parse("21/10/2019 10:00"), 2, 2, 5],
  ['Viewing on Flat', DateTime.parse("15/10/2019 10:00"), DateTime.parse("15/10/2019 11:00"), 1, 2, 1],
  ['Viewing', DateTime.parse("15/10/2019 11:00"), DateTime.parse("15/10/2019 15:00"), 1, 2, 2],
  ['Viewing', DateTime.parse("15/10/2019 12:00"), DateTime.parse("15/10/2019 13:00"), 1, 2, 3],
  ['Broken Boiler', DateTime.parse("15/10/2019 14:00"), DateTime.parse("15/10/2019 15:00"), 2, 1, 1],
  ['Landlord checkup', DateTime.parse("15/10/2019 09:00"), DateTime.parse("15/10/2019 12:00"), 3, 3, 4],
  ['Gary on Holiday', DateTime.parse("15/10/2019 09:00"), DateTime.parse("15/10/2019 12:00"), 4, 8, 2],
]
events.each do |title, start_time, end_time, event_type, property_id, user_id|
  Event.create(title: title,
               start: start_time,
               end: end_time,
               event_type: event_type,
               property_id: property_id,
               user_id: user_id )
end