Rails.application.routes.draw do
  resources :tenants
  resources :events
  devise_for :users
  resources :rooms
  resources :landlords
  resources :properties
  get 'welcome/index'

  root 'welcome#index'
end
