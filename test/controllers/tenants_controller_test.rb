require 'test_helper'

class TenantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tenant = tenants(:one)
  end

  test "should get index" do
    get tenants_url
    assert_response :success
  end

  test "should get new" do
    get new_tenant_url
    assert_response :success
  end

  test "should create tenant" do
    assert_difference('Tenant.count') do
      post tenants_url, params: { tenant: { course: @tenant.course, dob: @tenant.dob, email: @tenant.email, first_name: @tenant.first_name, guarantor_address: @tenant.guarantor_address, guarantor_email: @tenant.guarantor_email, guarantor_mobile: @tenant.guarantor_mobile, guarantor_name: @tenant.guarantor_name, guarantor_post_code: @tenant.guarantor_post_code, guarantor_relationship: @tenant.guarantor_relationship, mobile_number: @tenant.mobile_number, nationality: @tenant.nationality, student_id_number: @tenant.student_id_number, studying_at: @tenant.studying_at, surname: @tenant.surname } }
    end

    assert_redirected_to tenant_url(Tenant.last)
  end

  test "should show tenant" do
    get tenant_url(@tenant)
    assert_response :success
  end

  test "should get edit" do
    get edit_tenant_url(@tenant)
    assert_response :success
  end

  test "should update tenant" do
    patch tenant_url(@tenant), params: { tenant: { course: @tenant.course, dob: @tenant.dob, email: @tenant.email, first_name: @tenant.first_name, guarantor_address: @tenant.guarantor_address, guarantor_email: @tenant.guarantor_email, guarantor_mobile: @tenant.guarantor_mobile, guarantor_name: @tenant.guarantor_name, guarantor_post_code: @tenant.guarantor_post_code, guarantor_relationship: @tenant.guarantor_relationship, mobile_number: @tenant.mobile_number, nationality: @tenant.nationality, student_id_number: @tenant.student_id_number, studying_at: @tenant.studying_at, surname: @tenant.surname } }
    assert_redirected_to tenant_url(@tenant)
  end

  test "should destroy tenant" do
    assert_difference('Tenant.count', -1) do
      delete tenant_url(@tenant)
    end

    assert_redirected_to tenants_url
  end
end
